﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameGrid : MonoBehaviour
{
    public GridCell GridCellPrefab;
    public int NumberOfRows = 4;
    public int NumberOfColumns = 5;

    private GridCell[,] grid;
    
    private void Awake()
    {
        grid = new GridCell[NumberOfRows, NumberOfColumns];
        
        for (int i = 0; i < NumberOfRows; i++)
        {
            for (int j = 0; j < NumberOfColumns; j++)
            {
                GridCell cell = Instantiate(GridCellPrefab, transform);
                cell.SetPositions(i, j);
                cell.OnRuneAssigned += UpdateGrid;
                grid[i, j] = cell;
            }
        }
    }

    private void Start()
    {
        StartGridStates();
    }

    private void StartGridStates()
    {
        //Let only corner elements interactable
        for (int i = 0; i < NumberOfRows; i++)
        {
            for (int j = 0; j < NumberOfColumns; j++)
            {
                if (!(i == 0 || j == 0 || i == (NumberOfRows - 1) || j == NumberOfColumns - 1))
                {
                    grid[i, j].GetComponent<Button>().interactable = false;
                }
            }
        }
    }

    private void UpdateGrid(int x, int y)
    {
        bool buttonInteractability;
        
        //TODO: Optmize this later
        for (int i = 0; i < NumberOfRows; i++)
        {
            for (int j = 0; j < NumberOfColumns; j++)
            {
                buttonInteractability = false;
                var results = AdjacentElements(grid, i, j);
                foreach (GridCell result in results)
                {
                    if (result.IsAssigned)
                    {
                        buttonInteractability = true;
                        break;
                    }
                }

                grid[i, j].GetComponent<Button>().interactable = buttonInteractability;
            }
        }

        grid[x, y].GetComponent<Button>().interactable = true;

        var damageDealt = PatternChecker.GetDamageDealt(x, y, grid, NumberOfRows, NumberOfColumns);
        Debug.Log("Damage Dealt: " + damageDealt);
    }
    
    public static IEnumerable<T> AdjacentElements<T>(T[,] arr, int row, int column)
    {
        int rows = arr.GetLength(0);
        int columns = arr.GetLength(1);

        for (int j = row - 1; j <= row + 1; j++)
        for (int i = column - 1; i <= column + 1; i++)
            if (i >= 0 && j >= 0 && i < columns && j < rows && !(j == row && i == column))
                yield return arr[j, i];
    }
}
