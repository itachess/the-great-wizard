﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Rune : MonoBehaviour
{
    public string id;
    public Sprite GetButtonSprite()
    {
        return GetComponent<Image>().sprite;
    }
}
