using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "RunesConfig", menuName = "GameConfigs/RunesConfig", order = 0)]
public class RunesConfig : ScriptableObject {
    public List<SingleRuneConfig> RunesList;
}

[Serializable]
public class SingleRuneConfig {
    public Rune RunePrefab;
    public int Amount;
}