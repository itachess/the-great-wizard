﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class PoolController : MonoBehaviour
{
    public RunesConfig RunesConfig;
    public int RunesAmountToDraw;

    private List<Rune> runes;
    private List<Rune> runesPool;
    private int selectedRuneIndex = -1;

    public Rune GetSelectedRune()
    {
        if (selectedRuneIndex == -1)
        {
            return null;
        }
        
        Rune selectedRune = runesPool[selectedRuneIndex];

        ShowRunes();

        return selectedRune;
    }
    
    private void Start()
    {
        runes = new List<Rune>();
        runesPool = new List<Rune>();
        
        foreach (SingleRuneConfig singleRuneConfig in RunesConfig.RunesList)
        {
            for(int i = 0; i< singleRuneConfig.Amount; i++){
                runes.Add(singleRuneConfig.RunePrefab);
            }
        }

        ShowRunes();
    }

    private void ShowRunes()
    {
        selectedRuneIndex = -1;
        
        foreach (var rune in runesPool)
        {
            Destroy(rune.gameObject);
        }

        runesPool.Clear();

        for (int i = 0; i < RunesAmountToDraw; i++)
        {
            int runeIndex = i;
            Rune runePrefab = DrawRune();
            Rune rune = Instantiate(runePrefab, transform);

            rune.GetComponent<Button>().onClick.AddListener(() =>
            {
                SetRuneIndex(runeIndex);
            });

            runesPool.Add(rune);
        }
    }

    private Rune DrawRune(){
        Rune[] runesBkp = runes.ToArray();
        runes.Clear();

        //Shuffle all
        foreach (var rune in runesBkp.OrderBy(x => Random.Range(0, Int32.MaxValue)))
        {
            runes.Add(rune);
        }

        Rune drawnRune = runes[0];
        runes.RemoveAt(0);

        return drawnRune;
    }

    private void SetRuneIndex(int value)
    {
        selectedRuneIndex = value;
    }
}
