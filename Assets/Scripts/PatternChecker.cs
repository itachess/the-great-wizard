
using System.Text;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public static class PatternChecker {
    
    private static List<string> CurrentPatterns = new List<string>(){
        "(?=(A[a-zA-Z]{3}A))",
        "(?=(A[a-zA-Z]{5}A))"
    };
    //A C A B A B A E A E A C A C A B A B A D ZZZZZZZZZZ

    public static float GetDamageDealt(int x, int y, GridCell[,] grid, int MaxRows, int MaxCollumns){
        //Os patterns estão pegando 3 combinações a mais do que deveria nesse caso, acho que não vai ter um pattern pra todos os casos,
        //talvez precise de 1 pattern para cada tipo de combinação diferente dentro do board ?
        StringBuilder allGridText = new StringBuilder("ACABABAEAEACACABABAD");
        
        // for (int i = 0; i < MaxRows; i++)
        // {
        //     for (int j = 0; j < MaxCollumns; j++)
        //     {
        //         allGridText.Append(grid[i, j].currentId);
        //     }
        // }
        Debug.Log(allGridText);

        int matches = Regex.Matches(allGridText.ToString(), CurrentPatterns[0]).Count;
        Debug.Log(matches);
        int matches2 = Regex.Matches(allGridText.ToString(), CurrentPatterns[1]).Count;
        Debug.Log(matches2);
        return matches + matches2;
    }
}