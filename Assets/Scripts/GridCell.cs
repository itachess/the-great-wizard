﻿using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System;
using UnityEngine;
using UnityEngine.UI;

public class GridCell : MonoBehaviour
{
    public Action<int, int> OnRuneAssigned;
    public bool IsAssigned;

    private PoolController poolController;
    private Button button;
    private int x, y;
    public string currentId = "Z";

    private void Awake()
    {
        IsAssigned = false;
        button = GetComponent<Button>();
        button.onClick.AddListener(TryToAssignRune);
    }

    private void Start()
    {
        poolController = FindObjectOfType<PoolController>();
    }

    private void TryToAssignRune()
    {
        if (IsAssigned)
        {
            return;
        }
        
        Rune rune = poolController.GetSelectedRune();

        if (rune == null)
        {
            return;
        }

        Sprite runeSprite = rune.GetButtonSprite();
        currentId = rune.id;
        GetComponent<Image>().sprite = runeSprite;
        IsAssigned = true;
        OnRuneAssigned?.Invoke(x, y);
    }

    public void SetPositions(int x, int y)
    {
        currentId = "Z";
        this.x = x;
        this.y = y;
    }
}
